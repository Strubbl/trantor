package database

import "testing"

const (
	listID   = "1234567890"
	title    = "My title"
	username = "username"
)

func TestNewBookList(t *testing.T) {
	db, dbclose := testDbInit(t)
	defer dbclose()
	testCreateList(t, db)

	list, err := db.GetBookList(listID)
	if err != nil {
		t.Fatal("db.GetBookList() return an error: ", err)
	}
	if list.ListID != listID {
		t.Fatal("list id doesn't match: ", list.ListID, " <=> ", listID)
	}
	if list.Title != title {
		t.Fatal("title doesn't match: ", list.Title, " <=> ", title)
	}
}

func TestBookListAddDelBooks(t *testing.T) {
	db, dbclose := testDbInit(t)
	defer dbclose()

	testCreateList(t, db)
	testAddBook(t, db)

	err := db.AddBookToList(listID, book.ID)
	if err != nil {
		t.Fatal("db.AddBookToList() return an error: ", err)
	}

	list, err := db.GetBookList(listID)
	if err != nil {
		t.Fatal("db.GetBookList() return an error: ", err)
	}
	if len(list.Books) != 1 {
		t.Fatal("We got a un expected number of books: ", list.Books)
	}
	if list.Books[0].ID != book.ID {
		t.Fatal("book id doesn't match: ", list.Books[0].ID, " <=> ", book.ID)
	}

	lists, err := db.GetListsByBook(book.ID)
	if err != nil {
		t.Fatal("db.GetListsByBook() return an error: ", err)
	}
	if len(lists) != 1 {
		t.Fatal("We got a un expected number of lists: ", lists)
	}
	if lists[0].Books[0].Title != book.Title {
		t.Fatal("book id doesn't match: ", lists[0].Books[0].Title, " <=> ", book.Title)
	}

	err = db.DeleteBookFromList(listID, book.ID)
	if err != nil {
		t.Fatal("db.DeleteBookFromList() return an error: ", err)
	}

	list, err = db.GetBookList(listID)
	if err != nil {
		t.Fatal("db.GetBookList() return an error: ", err)
	}
	if len(list.Books) != 0 {
		t.Fatal("We got a un expected number of books: ", list.Books)
	}
}

func TestBookListByUser(t *testing.T) {
	db, dbclose := testDbInit(t)
	defer dbclose()
	testCreateList(t, db)

	lists, err := db.GetListsByUser(username)
	if err != nil {
		t.Fatal("db.GetBookListsByUser() return an error: ", err)
	}
	if len(lists) != 1 {
		t.Fatal("We got a un expected number of lists: ", lists)
	}
	if lists[0].ListID != listID {
		t.Fatal("list id doesn't match: ", lists[0].ListID, " <=> ", listID)
	}
}

func TestBookListUpdate(t *testing.T) {
	const otherTitle = "other title"

	db, dbclose := testDbInit(t)
	defer dbclose()
	testCreateList(t, db)

	err := db.UpdateBookList(listID, otherTitle, []string{})
	if err != nil {
		t.Fatal("db.UpdateBookList() return an error: ", err)
	}

	list, err := db.GetBookList(listID)
	if err != nil {
		t.Fatal("db.GetBookList() return an error: ", err)
	}
	if list.Title != otherTitle {
		t.Fatal("title doesn't match: ", list.Title, " <=> ", otherTitle)
	}
}

func testCreateList(t *testing.T, db DB) {
	err := db.AddUser(username, "pass")
	if err != nil {
		t.Fatal("db.AddUser() return an error: ", err)
	}
	err = db.NewBookList(listID, title, username, []string{})
	if err != nil {
		t.Fatal("db.NewBookList() return an error: ", err)
	}
}

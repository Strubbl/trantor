module gitlab.com/trantor/trantor

go 1.14

require (
	github.com/alecthomas/units v0.0.0-20201120081800-1786d5ef83d4 // indirect
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/go-pg/pg/v10 v10.7.3
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.1
	github.com/jmhodges/gocld2 v0.0.0-20180523203442-58c6ee0ad8b6
	github.com/meskio/epubgo v0.0.0-20160213181628-90dd5d78197f
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/prometheus/client_golang v1.8.0
	github.com/prometheus/common v0.15.0
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/vmihailenco/msgpack/v5 v5.1.0 // indirect
	golang.org/x/crypto v0.0.0-20201124201722-c8d3bf9c5392
	golang.org/x/sys v0.0.0-20201130072748-111129e158e2 // indirect
	golang.org/x/text v0.3.4 // indirect
)

# Imperial Library of Trantor

The Imperial Library of Trantor (also known as Galactic Library) is a 
repository management system of ebooks on ePub format.

You can check out the main development branch from GitLab at:

https://gitlab.com/trantor/

## Dependences

In order to run Trantor, you need to install the following packages:

* Go language
* postgresql (>= 9.6)

## Installation

### Create postgres DB

We can create the DB (using the postgres user) and activate `pg_trgm` extension
for it:
```
$ createdb trantor
$ echo "CREATE EXTENSION pg_trgm;"|psql trantor
```

### For admins ("for developers" below)

Now you can install Trantor itself:
```
$ git clone gitlab.com/trantor/trantor
$ cd trantor
$ go build
```

Generate a random session key:
```
$ base64 /dev/random | head -c 50
```

You can run it (using `/var/lib/trantor` for storage):
```
$ SESSION_KEY=<session> ./trantor -store /var/lib/trantor
```

The first time you run it, the database will be initialized. To initialize an admin user, include the -set-admin-user and -set-admin-pass variables
when you run it. The admin user will be initialized after the first time you run the application with these parameters, so there is no need to
include them on subsequent runs (but no harm in doing so).
```
# $GOPATH/bin/trantor -assets $GOPATH/src/pkg/gitlab.com/trantor/trantor/ -store /var/lib/trantor -set-admin-user 'admin' -set-admin-pass 'MY_ADMIN_PASSWORD'
```

Go to your browser to: http://localhost:8080

### For developers

Now you can compile Trantor:
```
$ go get .
$ go build
```

Now you can run it:
```
$ ./trantor  -set-admin-user 'admin' -set-admin-pass 'MY_ADMIN_PASSWORD'
```

Go to your browser to: http://localhost:8080

## Bugs

Please, report bugs in the gitlab issue tracker:  
https://gitlab.com/trantor/trantor/issues

## Rights

All the matterial of this project is under WTFPL as described on the LICENSE 
file with the exception of:
* css/bootstrap.min.css css/bootstra-responsive.min.css js/bootstrap.min.js 
  img/glyphicons-halflings-white.png img/glyphicons-halflings.png  
  From the bootstrap framework: http://twitter.github.com/bootstrap/
* js/jquery.js  
  From jquery library: http://jquery.com/
* img/bright_squares.png  
  From subtlepatterns: http://subtlepatterns.com/bright-squares/
* css/FredokaOne.ttf css/PTSerif.ttf  
  From Google Web Fonts: http://www.google.com/webfonts
* js/bootstrap-tokenfield.min.js dist/css/bootstrap-tokenfield.min.css  
  From Bootstrap Tokenfield: https://github.com/sliptree/bootstrap-tokenfield
